// Copyright 2022 Zheleztsov N. (WEB-12)

/*
 * Подсчитать кол-во единичных бит в входном числе, стоящих на четных
 * позициях. Позиции битов нумеруются с 0. Необходимо использование
 * битовых операций. Использование арифметических операций запрещено.
 */

#include <cstdint>
#include <cstdlib>
#include <iostream>

uint32_t get_number_of_even_ones(uint32_t num) {
  uint32_t counter = 0;
  while (num) {
    if (num & 1) {
        ++counter;
    }

    num >>= 2;
  }

  return counter;
}

int main() {
  uint32_t num(0);
  std::cin >> num;
  std::cout << get_number_of_even_ones(num);
  return EXIT_SUCCESS;
}

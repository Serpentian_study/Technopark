// Copyright 2021 Zheleztsov N. (ML-12)

/*
 * If the number contains only one bit with a value of 1,
 * output OK. Otherwise, FAIL.
 */

#include <iostream>

bool is_one_bit(uint32_t value) {
  return ((value == 0) || ((value - 1) & value)) ? false : true;
}

int main() {
  uint32_t value(0);

  std::cin >> value;
  std::cout << (is_one_bit(value) ? "OK" : "FAIL");

  return 0;
}

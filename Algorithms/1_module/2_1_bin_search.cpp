// Copyright 2022 Zheleztsov N. (WEB-12)

/*
 * Дан отсортированный массив целых чисел A[0..n-1] и массив целых чисел
 * B[0..m-1]. Для каждого элемента массива B[i] найдите минимальный индекс k
 * минимального элемента массива A, равного или превосходящего B[i]: A[k] >=
 * B[i]. Если такого элемента нет, выведите n. Время работы поиска k для
 * каждого элемента B[i]: O(log(k)). n, m ≤ 10000.
 */

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <stdexcept>

#define BOUND_IDX (bound - 1)

template <typename T>
uint32_t binary_search(const T* const arr, const size_t& size, const T& value) {
  uint32_t left = 0, right = size - 1;
  while (left < right) {
    size_t mid = left + (right - left) / 2;
    if (arr[mid] < value) {
      left = mid + 1;
    } else {
      right = mid;
    }
  }

  return arr[left] < value ? size : left;
}

template <typename T>
uint32_t exp_search(const T* const arr, const size_t& size, const T& value) {
  uint32_t bound = 1;
  while (bound <= size && arr[BOUND_IDX] < value) {
    bound <<= 1;
  }

  if (bound != 1) {
    bound >>= 1;
  }

  return BOUND_IDX + binary_search(&arr[BOUND_IDX], size - BOUND_IDX, value);
}

template <typename T>
void min_idx(const T* const arr_a, const size_t& size_a, const T* const arr_b,
             const size_t& size_b, uint32_t* res) {
  assert(res);
  for (size_t i = 0; i < size_b; ++i) {
    res[i] = exp_search(arr_a, size_a, arr_b[i]);
  }
}

int main() {
  size_t n(0), m(0);
  std::cin >> n >> m;

  int *arr_a = new int[n], *arr_b = new int[m];
  for (size_t i = 0; i < n; ++i) {
    std::cin >> arr_a[i];
  }

  for (size_t i = 0; i < m; ++i) {
    std::cin >> arr_b[i];
  }

  uint32_t* res = new uint32_t[m];
  min_idx(arr_a, n, arr_b, m, res);
  for (size_t i = 0; i < m; ++i) {
    std::cout << res[i] << ' ';
  }

  return EXIT_SUCCESS;
}

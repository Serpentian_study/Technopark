// Copyright 2022 Zheleztsov N. (WEB-12)

/*
 * Дан массив целых чисел А[0..n-1]. Известно, что на интервале [0, m]
 * значения массива строго возрастают, а на интервале [m, n-1] строго
 * убывают. Найти m за O( log m ). 2 ≤ n ≤ 10000.
 */

#include <iostream>
#include <stdexcept>

template <class T>
size_t bin_search(const T *const a, const size_t size) {
  size_t begin = 0, end = size - 1;
  while (begin < end) {
    size_t mid = begin + (end - begin) / 2;
    if (a[mid] < a[end]) {
      begin = mid + 1;
    } else {
      end = mid;
    }
  }

  return begin;
}

template <class T>
size_t exponential_search(const T *const a, const size_t size) {
  if (!size) {
    throw(std::invalid_argument("Size must not be equal to zero"));
  }

  size_t begin = 1, end = begin << 1;
  while (end <= size && a[begin - 1] < a[end - 1]) {
    begin <<= 1;
    end <<= 1;
  }

  if (begin != 1) {
    begin >>= 1;
  }

  return begin - 1 + bin_search(&a[begin - 1], std::min(end, size) - begin + 1);
}

int main(int argc, char *argv[]) {
  size_t size(0);
  std::cin >> size;

  int arr[size];
  for (size_t i = 0; i < size; ++i) {
    std::cin >> arr[i];
  }

  std::cout << exponential_search(arr, size);
  return 0;
}

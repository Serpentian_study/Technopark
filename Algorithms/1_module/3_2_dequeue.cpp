// Copyright 2021 Zheleztsov N. (ML-12)

/*
 * Реализовать дек с динамическим зацикленным буфером.
 * Обрабатывать команды push * и pop *.
 */

#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <memory>
#include <type_traits>

#define MIN_CAPACITY 4

template <class T, class Allocator = std::allocator<T> >
class Deque {
 public:
  ////////////////////////
  //    Member types    //
  ////////////////////////

  typedef T value_type;
  typedef Allocator allocator_type;
  typedef ssize_t size_type;
  typedef value_type& reference;
  typedef const value_type& const_reference;
  typedef typename std::allocator_traits<Allocator>::pointer pointer;
  typedef
      typename std::allocator_traits<Allocator>::const_pointer const_pointer;
  typedef typename std::allocator_traits<Allocator> alloc_traits;

  ////////////////////////
  //  Member functions  //
  ////////////////////////

  // (constructor)
  Deque();
  explicit Deque(size_type count);
  Deque(const Deque& other) = delete;
  Deque(Deque&& other) = delete;

  // (destructor)
  ~Deque();

  // operator=
  Deque& operator=(const Deque& other) = delete;
  Deque& operator=(Deque&& other) = delete;

  // Element access
  const_reference back() { return _data[_back]; }
  const_reference front() { return _data[_front]; }

  // Capacity
  bool empty() const noexcept { return !_size; }
  size_type size() const noexcept { return _size; }

  // Modifiers
  void push_back(const T& value);
  void push_front(const T& value);

  void pop_back();
  void pop_front();

 private:
  template <typename K>
  K remainder(const K& a, const K& b);

  void reallocate();

  pointer _data = nullptr;
  allocator_type _alloc = Allocator();

  // Maybe it's overhead to store size and capacity
  size_type _size = 0;
  size_type _cap = 0;

  size_type _front = 0;
  size_type _back = -1;
};

template <class T, class Allocator>
template <typename K>
inline K Deque<T, Allocator>::remainder(const K& a, const K& b) {
  static_assert(std::is_integral<K>::value, "Integral is requered");
  T q = (a < 0) ? ((a / b) * (-1) - 1) : a / b;
  return a - b * q;
}

template <class T, class Allocator>
Deque<T, Allocator>::Deque() : _cap(MIN_CAPACITY) {
  _data = Deque::alloc_traits::allocate(_alloc, _cap);
}

template <class T, class Allocator>
Deque<T, Allocator>::Deque(Deque::size_type count) : _cap(count) {
  _data = Deque::alloc_traits::allocate(_alloc, _cap);
}

template <class T, class Allocator>
inline void Deque<T, Allocator>::reallocate() {
  T* temp = _data;
  size_t old_cap = _cap;

  _cap = _cap ? _cap * 2 : MIN_CAPACITY;
  _data = std::allocator_traits<Allocator>::allocate(_alloc, _cap);

  for (Deque::size_type j = 0, i = _front; j < _size; ++i, ++j)
    Deque::alloc_traits::construct(_alloc, _data + j,
                                   temp[remainder(i, _size)]);

  _front = 0, _back = _front + _size - 1;
  Deque::alloc_traits::deallocate(_alloc, temp, old_cap);
}

template <class T, class Allocator>
void Deque<T, Allocator>::push_back(const T& value) {
  if (_size == _cap) {
    reallocate();
  }

  Deque::size_type new_back_idx = remainder(_back + 1, _cap);
  Deque::alloc_traits::construct(_alloc, _data + new_back_idx, value);

  _back = new_back_idx;
  if (!_size) {
    _front = _back;
  }

  ++_size;
}

template <class T, class Allocator>
void Deque<T, Allocator>::push_front(const T& value) {
  if (_size == _cap) {
    reallocate();
  }

  Deque::size_type new_front_idx = remainder(_front - 1, _cap);
  Deque::alloc_traits::construct(_alloc, _data + new_front_idx, value);

  _front = new_front_idx;
  if (!_size) {
    _back = _front;
  }

  ++_size;
}

template <class T, class Allocator>
void Deque<T, Allocator>::pop_back() {
  Deque::alloc_traits::destroy(_alloc, _data + _back);
  _back = remainder(_back - 1, _cap);
  --_size;
}

template <class T, class Allocator>
void Deque<T, Allocator>::pop_front() {
  Deque::alloc_traits::destroy(_alloc, _data + _front);
  _front = remainder(_front + 1, _cap);
  --_size;
}

template <class T, class Allocator>
inline Deque<T, Allocator>::~Deque() {
  Deque::alloc_traits::deallocate(_alloc, _data, _cap);
}

int main() {
  uint num_of_cmds = 0;
  std::cin >> num_of_cmds;

  Deque<int> deque;
  int a = 0, b = 0;

  bool is_ok = true;
  for (uint i = 0; i < num_of_cmds; ++i) {
    std::cin >> a >> b;
    if (a == 1) {
      deque.push_front(b);
      continue;

    } else if (a == 2) {
      int value = deque.empty() ? -1 : deque.front();
      if (value == b) {
        if (b != -1) {
          deque.pop_front();
        }
        continue;
      } else {
        is_ok = false;
        break;
      }

    } else if (a == 3) {
      deque.push_back(b);
      continue;

    } else if (a == 4) {
      int value = deque.empty() ? -1 : deque.back();
      if (value == b) {
        if (b != -1) {
          deque.pop_back();
        }
        continue;
      } else {
        is_ok = false;
        break;
      }

    } else {
      return EXIT_FAILURE;
    }
  }

  std::cout << (is_ok ? "YES" : "NO");
  return EXIT_SUCCESS;
}

// Copyright 2021 Zheleztsov N. (ML-12)

/*
 * There is a log file that stores pairs for N users (User ID, site traffic).
 *
 * Write a program that selects the K users who visited the site most often
 * and displays them in ascending order of attendance. The number of visits
 * and user IDs are not repeated.
 *
 * Requirements: running time O (N * logK), where N is the number of users.
 * Limit on heap size O (K).
 *
 * Input data format: First, N and K are entered, then pairs.
 * Output data format: User IDs in ascending order of attendance
 */

/////////////////////////////////////////////////////////////////
//            Implementation of the containers                 //
/////////////////////////////////////////////////////////////////

// Have already implemented STL vector
// https://github.com/NikZheleztsov/Vector-implementation

//==============//
//  Iterator.h  //
//==============//

// for iter tags
#include <iterator>

template <class T>
class Random_access_iterator {
  T *_pos = nullptr;

 public:
  typedef Random_access_iterator Iter;
  typedef Random_access_iterator &Iter_ref;

  typedef std::random_access_iterator_tag iterator_category;
  typedef T value_type;
  typedef std::ptrdiff_t difference_type;
  typedef T *pointer;
  typedef T &reference;

  Random_access_iterator(pointer pos) : _pos(pos) {}

  bool operator==(Iter const &other) const { return (_pos == other._pos); }
  bool operator!=(Iter const &other) const { return (_pos != other._pos); }
  reference operator*() const { return *_pos; }
  pointer operator->() const { return _pos; };
  Iter operator++() {
    ++_pos;
    return *this;
  };
  Iter operator++(int) {
    auto tmp = *this;
    ++_pos;
    return tmp;
  }
  Iter operator--() {
    --_pos;
    return *this;
  };
  Iter operator--(int) {
    auto tmp = *this;
    --_pos;
    return tmp;
  }
  reference operator[](size_t &index) const { return _pos[index]; }
  Iter operator+(difference_type const &diff) { return Iter(_pos + diff); }
  template <typename L>
  friend Iter operator+(difference_type const &diff,
                        Random_access_iterator<L> const &it);
  Iter operator-(difference_type const &diff) const {
    return Iter(_pos - diff);
  }
  difference_type operator-(Iter const &other) const {
    return std::distance(other._pos, _pos);
  }
  Iter_ref operator+=(difference_type const &diff) {
    _pos += diff;
    return *this;
  }
  Iter_ref operator-=(difference_type const &diff) {
    _pos -= diff;
    return *this;
  }
  bool operator>(Iter const &other) const { return _pos > other._pos; }
  bool operator>=(Iter const &other) const { return _pos >= other._pos; }
  bool operator<(Iter const &other) const { return _pos < other._pos; }
  bool operator<=(Iter const &other) const { return _pos <= other._pos; }
};

template <class T>
inline Random_access_iterator<T> operator+(
    std::ptrdiff_t const &diff, Random_access_iterator<T> const &it) {
  return Random_access_iterator<T>(it._pos + diff);
}

//============//
//  Vector.h  //
//============//

#include <cstdlib>
#include <limits>
#include <memory>
#include <stdexcept>

// Non-member functions are not implemented!
template <class T, class Allocator = std::allocator<T>>
class Vector {
  T *_data = nullptr;
  size_t _size = 0;
  size_t _cap = 0;  // real size; capacity() !!! return _cap - 1
  Allocator _alloc = Allocator();

  void reallocate(bool is_full_copy = false, size_t new_cap = 0);

 public:
  typedef T value_type;
  typedef Allocator allocator_type;
  typedef size_t size_type;
  typedef std::ptrdiff_t difference_type;
  typedef value_type &reference;
  typedef const value_type &const_reference;
  typedef typename std::allocator_traits<Allocator>::pointer pointer;
  typedef
      typename std::allocator_traits<Allocator>::const_pointer const_pointer;
  typedef Random_access_iterator<T> iterator;
  typedef const Random_access_iterator<T> const_iterator;
  typedef typename std::allocator_traits<Allocator> alloc_traits;

  ////////////////////////
  //  Member functions  //
  ////////////////////////

  // Constructor
  Vector();
  explicit Vector(size_type count);
  Vector(const Vector &other);
  Vector(Vector &&other);
  Vector(std::initializer_list<T> init);
  template <class InputIt>
  Vector(InputIt first, InputIt last);

  // Operator =
  Vector &operator=(const Vector &other);
  Vector &operator=(Vector &&other);
  Vector &operator=(std::initializer_list<T> ilist);

  // Iterators
  iterator begin() { return iterator(this->_data); }
  iterator end() { return iterator(this->_data + _size); }

  // Element access
  reference at(size_type pos);
  reference operator[](size_type pos);
  reference front() { return *begin(); }
  reference back() { return *(end() - 1); }
  T *data() noexcept { return _data; };

  // Capacity
  bool empty() const { return _size == 0; }
  size_type size() const { return _size; }
  size_type capacity() const { return _cap - 1; }
  size_type max_size() const;
  void reserve(size_type new_cap);
  void shrink_to_fit();

  // Modifiers
  void clear();

  iterator insert(iterator pos, const T &value);
  iterator insert(const_iterator pos, size_type count, const T &value);
  template <class InputIt, typename = std::_RequireInputIter<InputIt>>
  iterator insert(const_iterator pos, InputIt first, InputIt last);
  iterator insert(const_iterator pos, std::initializer_list<T> ilist);

  iterator erase(iterator pos);
  iterator erase(iterator first, iterator last);

  void push_back(const T &value);
  void pop_back();
  void resize(size_type count, T value = T());
  void swap(Vector &other);

  // Destructor
  ~Vector();
};

// inserts elements from range [first, last) before pos
// According to cppref the behavior is undefined if first and last are iterators
// into *this STL vector throw except if len < 0
template <class T, class Allocator>
template <class InputIt, typename>
typename Vector<T, Allocator>::iterator Vector<T, Allocator>::insert(
    Vector::const_iterator pos, InputIt first, InputIt last) {
  Vector temp(*this);
  std::ptrdiff_t len = last - first;
  if (len < 0 || len > max_size())
    throw std::length_error("vector::_M_range_insert");

  auto it = this->begin();
  if (_size + len >= _cap - 1) reallocate(0, _size + len + 1);

  Vector::difference_type i = 0, j = 0;

  for (; it < pos; ++i, ++it)
    Vector::alloc_traits::construct(_alloc, _data + i, temp._data[i]);

  Vector::iterator ret(_data + i + j);

  for (; j < len; ++j, ++first)
    Vector::alloc_traits::construct(_alloc, _data + i + j, *first);

  for (; i < temp.size() + 1; i++)
    Vector::alloc_traits::construct(_alloc, _data + i + j, temp._data[i - 1]);

  _size += len;
  return ret;
}

//==============//
//  Vector.cpp  //
//==============//

// it's already cap + 1 // only for inside use
template <class T, class Allocator>
inline void Vector<T, Allocator>::reallocate(bool is_full_copy,
                                             size_t new_cap) {
  Vector temp(*this);

  if (new_cap == 0) {
    if (_cap % 2 == 0)
      new_cap = 1.5 * _cap;
    else
      new_cap = 1.5 * _cap + 1;
  }

  Vector::alloc_traits::deallocate(_alloc, _data, _cap);
  _data = std::allocator_traits<Allocator>::allocate(_alloc, new_cap);
  _cap = new_cap;

  if (_size > _cap - 1) _size = _cap - 1;

  if (is_full_copy)
    for (Vector::difference_type i = 0; i < temp._size && i < _size; i++)
      Vector::alloc_traits::construct(_alloc, _data + i, temp._data[i]);
}

template <class T, class Allocator>
inline Vector<T, Allocator>::Vector() : _size(0), _cap(1) {
  _data = std::allocator_traits<Allocator>::allocate(_alloc, 1);
}

template <class T, class Allocator>
inline Vector<T, Allocator>::Vector(size_type count)
    : _size(0), _cap(count + 1) {
  _data = Vector::alloc_traits::allocate(_alloc, count + 1);
}

template <class T, class Allocator>
inline Vector<T, Allocator>::Vector(const Vector &other)
    : _size(other._size), _cap(other._cap) {
  _data = Vector::alloc_traits::allocate(_alloc, other._cap);
  for (Vector::difference_type i = 0; i < other._size; i++)
    Vector::alloc_traits::construct(_alloc, _data + i, other._data[i]);
}

template <class T, class Allocator>
inline Vector<T, Allocator>::Vector(Vector &&other) {
  _size = other._size;
  _cap = other._cap;
  _data = other._data;
  other._data = nullptr;
}

template <class T, class Allocator>
inline Vector<T, Allocator>::Vector(std::initializer_list<T> init) {
  _size = init.size();
  _cap = init.size() + 1;
  _data = Vector::alloc_traits::allocate(_alloc, _size + 1);

  auto it = init.begin();
  for (Vector::difference_type i = 0; i < init.size(); i++, it++)
    Vector::alloc_traits::construct(_alloc, _data + i, *it);
}

// if dist < 0 throw std::length err
template <class T, class Allocator>
template <class InputIt>
inline Vector<T, Allocator>::Vector(InputIt first, InputIt last) {
  auto dist = last - first;  // can't be < 0
  _data = Vector::alloc_traits::allocate(_alloc, _cap);

  if (dist < 0 || dist > max_size())
    throw std::length_error("cannot create std::vector larger than max_size()");

  for (Vector::difference_type i = 0; i < dist; ++i, ++first)
    Vector::alloc_traits::construct(_alloc, _data + i, *first);

  _size = std::abs(dist);
  _cap = std::abs(dist + 1);
}

template <class T, class Allocator>
inline Vector<T, Allocator> &Vector<T, Allocator>::operator=(
    const Vector &other) {
  Vector::alloc_traits::deallocate(_alloc, _data, _cap);
  _data = Vector::alloc_traits::allocate(_alloc, other._cap);
  for (Vector::difference_type i = 0; i < _size; i++)
    Vector::alloc_traits::construct(_alloc, _data + i, other._data[i]);

  _size = other._size;
  _cap = other._cap;

  return *this;
}

template <class T, class Allocator>
inline Vector<T, Allocator> &Vector<T, Allocator>::operator=(Vector &&other) {
  _size = other._size;
  _cap = other._cap;
  _data = other._data;
  other._data = nullptr;

  return *this;
}

template <class T, class Allocator>
inline Vector<T, Allocator> &Vector<T, Allocator>::operator=(
    std::initializer_list<T> ilist) {
  Vector::alloc_traits::deallocate(_alloc, _data, _cap);
  _data = Vector::alloc_traits::allocate(_alloc, _size + 1);

  auto it = ilist.begin();
  for (Vector::difference_type i = 0; i < ilist.size(); i++, it++)
    Vector::alloc_traits::construct(_alloc, _data + i, *it);

  _size = ilist.size();
  _cap = ilist.size() + 1;

  return *this;
}

template <class T, class Allocator>
inline typename Vector<T, Allocator>::reference
Vector<T, Allocator>::operator[](Vector::size_type pos) {
  return this->_data[pos];
}

template <class T, class Allocator>
inline typename Vector<T, Allocator>::reference Vector<T, Allocator>::at(
    Vector::size_type pos) {
  if (pos >= _size)
    throw std::out_of_range("Pos value is more than container's size");

  return (*this)[pos];
}

template <class T, class Allocator>
inline typename Vector<T, Allocator>::size_type Vector<T, Allocator>::max_size()
    const {
  const size_t dif_max = std::numeric_limits<std::ptrdiff_t>::max();
  const size_t aloc_max = Vector::alloc_traits::max_size(_alloc);
  return std::min(dif_max, aloc_max);
}

template <class T, class Allocator>
void Vector<T, Allocator>::reserve(Vector::size_type new_cap) {
  if (new_cap > max_size()) throw std::length_error("new_cap > max_size()");

  if (new_cap > _cap - 1) reallocate(true, new_cap + 1);

  _cap = new_cap + 1;
}

template <class T, class Allocator>
inline void Vector<T, Allocator>::clear() {
  for (Vector::difference_type i = 0; i < _size; i++)
    Vector::alloc_traits::destroy(_alloc, _data + i);

  _size = 0;
  // doesn't change the capacity
}

template <class T, class Allocator>
void Vector<T, Allocator>::shrink_to_fit() {
  if (_cap - 1 > _size) reallocate(true, _size + 1);
}

// inserts value before pos
template <class T, class Allocator>
typename Vector<T, Allocator>::iterator Vector<T, Allocator>::insert(
    Vector::iterator pos, const T &value) {
  Vector temp(*this);
  auto it = this->begin();
  if (_size + 1 > _cap - 1) reallocate();  // invalidating of iterators

  Vector::difference_type i = 0;
  for (; it < pos; ++i, ++it)
    Vector::alloc_traits::construct(_alloc, _data + i, temp._data[i]);

  Vector::iterator ret(_data + i);
  Vector::alloc_traits::construct(_alloc, _data + i, value);

  ++i;
  for (; i < temp.size() + 1; i++)
    Vector::alloc_traits::construct(_alloc, _data + i, temp._data[i - 1]);

  _size += 1;
  return ret;
}

// inserts count copies of the value before pos
template <class T, class Allocator>
typename Vector<T, Allocator>::iterator Vector<T, Allocator>::insert(
    Vector::const_iterator pos, Vector::size_type count, const T &value) {
  Vector temp(*this);
  auto it = this->begin();
  if (_size + count >= _cap - 1)
    reallocate(0, _size + count + 1);  // invalidating

  Vector::difference_type i = 0;
  for (; it < pos; ++i, ++it)
    Vector::alloc_traits::construct(_alloc, _data + i, temp._data[i]);

  Vector::size_type j = 0;
  Vector::iterator ret(_data + i + j);
  for (; j < count; j++)
    Vector::alloc_traits::construct(_alloc, _data + i + j, value);

  for (; i < temp.size() + 1; i++)
    Vector::alloc_traits::construct(_alloc, _data + i + j, temp._data[i]);

  _size += count;
  return ret;
}

// inserts elements from initializer list before pos
template <class T, class Allocator>
typename Vector<T, Allocator>::iterator Vector<T, Allocator>::insert(
    Vector::const_iterator pos, std::initializer_list<T> ilist) {
  Vector temp(*this);
  auto it = this->begin();
  if (_size + ilist.size() >= _cap - 1) reallocate(0, _size + ilist.size() + 1);

  Vector::difference_type i = 0, j = 0;
  for (; it < pos; ++i, ++it)
    Vector::alloc_traits::construct(_alloc, _data + i, temp._data[i]);

  Vector::iterator ret(_data + i + j);
  auto it_list = ilist.begin();
  for (; j < ilist.size(); j++, it_list++)
    Vector::alloc_traits::construct(_alloc, _data + i + j, *it_list);

  for (; i < temp.size() + 1; i++)
    Vector::alloc_traits::construct(_alloc, _data + i + j, temp._data[i]);

  _size += ilist.size();
  return ret;
}

template <class T, class Allocator>
typename Vector<T, Allocator>::iterator Vector<T, Allocator>::erase(
    typename Vector<T, Allocator>::iterator pos) {
  Vector::difference_type diff = pos - begin();
  for (int i = 0; pos < end(); pos++, i++)
    Vector::alloc_traits::construct(_alloc, _data + diff + i,
                                    *(_data + diff + i + 1));
  _size--;

  return end();
}

// [first, last)
template <class T, class Allocator>
typename Vector<T, Allocator>::iterator Vector<T, Allocator>::erase(
    typename Vector<T, Allocator>::iterator first,
    typename Vector<T, Allocator>::iterator last) {
  if (empty()) return last;

  auto f_pos = first - begin();
  auto dif = last - first;

  for (int i = 0; first < last; ++first, i++)
    Vector::alloc_traits::construct(_alloc, &(*first),
                                    *(_data + f_pos + dif + i));

  _size -= dif;
  return end();
}

template <class T, class Allocator>
void Vector<T, Allocator>::push_back(const T &value) {
  if (_size + 1 > _cap - 1) reallocate(true);

  Vector::alloc_traits::construct(_alloc, _data + _size, value);
  _size++;
}

template <class T, class Allocator>
inline void Vector<T, Allocator>::pop_back() {
  Vector::alloc_traits::destroy(_alloc, _data + _size - 1);
  _size--;
}

template <class T, class Allocator>
void Vector<T, Allocator>::resize(Vector::size_type count, T value) {
  if (count > _size) {
    // default append
    if (count > _cap - 1) reallocate(true, count + 1);

    for (size_t j = 0; j < count; j++)
      Vector::alloc_traits::construct(_alloc, _data + _size + j, value);

  } else {
    // erase at the end
    for (Vector::difference_type i = 0; i < _size - count; i++)
      Vector::alloc_traits::destroy(_alloc, _data + _size - 1 - i);
  }

  _size = count;
}

template <class T, class Allocator>
inline void Vector<T, Allocator>::swap(Vector &other) {
  Vector other_temp(std::move(other));
  other = std::move(*this);
  *this = std::move(other_temp);
}

template <class T, class Allocator>
inline Vector<T, Allocator>::~Vector() {
  for (Vector::size_type i = 0; i < _size; ++i) _data[i].~T();

  Vector::alloc_traits::deallocate(_alloc, _data, _cap);
}

//////////////////////
//      Heap        //
//////////////////////

// Min-max heap if Compare = std::less<T>
// else Max-min

// Requirements for the Container:
//
// The container must satisfy the requirements of SequenceContainer
// Additionaly, it must provide the following functions with the
// usual semantics:
// * push_back(...)
// * pop_back()
// * data()
//
// Our vector implementation doesn't fully satisfy the requirements of
// SequenceContainer (non-member functions are not implemented), but
// it's ok)
//
// Heap, for its part, doesn't even satisfy the requiremetns of
// the Container (from c++ named requirements), because I'm too lazy)
template <typename T, class Compare = std::less<T>, class Container = Vector<T>>
class Heap {
 public:
  typedef Container container_type;
  typedef typename Container::value_type value_type;
  typedef typename Container::size_type size_type;
  typedef typename Container::reference reference;
  typedef typename Container::const_reference const_reference;
  typedef typename Container::pointer pointer;
  typedef typename Container::const_pointer const_pointer;

  Heap() = default;
  explicit Heap(size_type count, const Compare &_comp = Compare())
      : arr(count), comp(_comp) {}
  explicit Heap(const Container &&_arr, const Compare &_comp)
      : arr(std::forward<Container>(_arr)), comp(_comp) {}
  ~Heap() = default;

  void push(const T &value);
  size_type size() { return arr.size(); }
  bool empty() { return arr.empty(); }
  size_type max_size() { return arr.max_size(); }
  pointer data() { return arr.data(); }

  // undefined behavior if arr.empty():
  void pop_min();
  reference peek_min() { return arr.front(); }

 private:
  Container arr;
  Compare comp;

  void build_heap();
  void sift_down(Heap::size_type ind);
  void sift_up(Heap::size_type ind);
};

template <typename T, class Compare, class Container>
void Heap<T, Compare, Container>::sift_down(Heap::size_type ind) {
  // anything but not ind
  Heap::size_type max = max_size();
  while (max != ind) {
    Heap::size_type left = 2 * ind + 1, right = 2 * ind + 2;
    max = ind;

    if (left < arr.size() && comp(arr[left], arr[max])) max = left;
    if (right < arr.size() && comp(arr[right], arr[max])) max = right;

    if (max != ind) {
      std::swap(arr[ind], arr[max]);
      ind = max;
      // anything but not ind
      max = max_size();
    }
  }
}

template <typename T, class Compare, class Container>
void Heap<T, Compare, Container>::sift_up(Heap::size_type ind) {
  while (ind > 0) {
    Heap::size_type parent = (ind - 1) / 2;
    if (!comp(arr[ind], arr[parent])) return;

    std::swap(arr[ind], arr[parent]);
    ind = parent;
  }
}

template <typename T, class Compare, class Container>
void Heap<T, Compare, Container>::build_heap() {
  for (Heap::size_type i = arr.size() / 2 - 1; i >= 0; --i) sift_down(i);
}

template <typename T, class Compare, class Container>
void Heap<T, Compare, Container>::push(const T &value) {
  arr.push_back(value);
  sift_up(arr.size() - 1);
}

template <typename T, class Compare, class Container>
void Heap<T, Compare, Container>::pop_min() {
  arr[0] = arr.back();
  arr.pop_back();

  if (!arr.empty()) sift_down(0);
}

/////////////////////////////////////////////////////////////////
//                 Completing of the task                      //
/////////////////////////////////////////////////////////////////

class User {
  int64_t id = -1;
  int64_t attendance = -1;

 public:
  User() = default;
  explicit User(int64_t _id, int64_t _att) : id(_id), attendance(_att) {}
  int64_t &get_id() { return id; }
  ~User() = default;

  bool friend operator<(const User &, const User &);
};

bool operator<(const User &first, const User &second) {
  return first.attendance < second.attendance;
}

// The comparison function isn't needed in Heap_wrapper
// We need min-max heap for our solution which
// is the option by default
//
// But let it be here in case the task requres it
// (the task isn't very clear about it)
template <class Compare = std::less<User>>
class Heap_wrapper {
 public:
  typedef Heap<User> heap;

  Heap_wrapper() = delete;
  Heap_wrapper(heap::size_type _k, const Compare &_comp = Compare())
      : hp(_k), k(_k), comp(_comp) {}
  ~Heap_wrapper() = default;

  void push(const User &value);
  void pop_min() { hp.pop_min(); }
  typename heap::reference peek_min() { return hp.peek_min(); }
  typename heap::size_type size() { return hp.size(); }
  typename heap::pointer data() { return hp.data(); }

 private:
  Heap<User, Compare> hp;
  heap::size_type k = 0;
  Compare comp;
};

template <class Compare>
void Heap_wrapper<Compare>::push(const User &value) {
  if (k > hp.size())
    hp.push(value);

  else if (comp(hp.peek_min(), value)) {
    hp.pop_min();
    hp.push(value);
  }
}

// sort.h
#include <cstring>

// Can be used instead of std::less everywhere
// But there's no practical benefit from this
template <typename T>
class is_less {
 public:
  bool operator()(const T &l, const T &r) const { return l < r; }
};

namespace sort {

template <typename T, class Compare>
void merge(T *arr, size_t l, size_t m, size_t r, Compare comp) {
  T *sorted = new T[r - l + 1];
  size_t left_ind = l, right_ind = m + 1, sorted_ind = 0;

  // Have we reached the end of any array?
  while (left_ind != m + 1 && right_ind != r + 1) {
    if (comp(arr[left_ind], arr[right_ind])) {
      sorted[sorted_ind] = arr[left_ind];
      ++left_ind;

    } else {
      sorted[sorted_ind] = arr[right_ind];
      ++right_ind;
    }

    ++sorted_ind;
  }

  // Copy all remaining elements of non-empty array
  if (left_ind == m + 1) {
    // Copy all remaining elements of right sub-array
    for (size_t i = right_ind; i < r + 1; ++i, ++sorted_ind)
      sorted[sorted_ind] = arr[i];

  } else {
    // Copy all remaining elements of left sub-array
    for (size_t i = left_ind; i < m + 1; ++i, ++sorted_ind)
      sorted[sorted_ind] = arr[i];
  }

  std::memcpy(arr + l, sorted, sizeof(T) * (r - l + 1));
  delete[] sorted;
}

template <typename T, class Compare = std::less<T>>
void merge_sort(T *arr, size_t l, size_t r, Compare comp = Compare()) {
  if (l < r) {
    size_t m = (r + l) / 2;
    merge_sort<T, Compare>(arr, l, m);
    merge_sort<T, Compare>(arr, m + 1, r);
    merge<T, Compare>(arr, l, m, r, comp);
  }
}

}  // namespace sort

// main.cpp
#include <cassert>
#include <iostream>

int main(int argc, char **argv) {
  size_t n = 0, k = 0;
  std::cin >> n >> k;
  assert(!(k > n));

  Heap_wrapper<std::greater<User>> hp(k);
  for (size_t i = 0; i < n; ++i) {
    int64_t temp_id = 0, temp_att = 0;
    std::cin >> temp_id >> temp_att;
    hp.push(User(temp_id, temp_att));
  }

  // No need for correct work of heap now
  size_t size = hp.size();
  User *data = hp.data();
  // If size == 1 merge will do nothing
  sort::merge_sort(data, 0, size - 1);

  for (size_t i = 0; i < size; ++i)
    std::cout << std::to_string(data[i].get_id()) << " ";

  return 0;
}

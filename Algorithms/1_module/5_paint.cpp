// Copyright 2021 Zheleztsov N. (ML-12)

/*
** Variant 5.3
**
** N segments were painted. The coordinates of the left
** and right ends of each segment (Li and Ri) are known.
** Find the length of the colored part of a number line
*/

#include <cstring>
#include <iostream>

/////////////////////////////
//      Merge sort         //
/////////////////////////////

template <typename T>
class is_less {
 public:
  bool operator()(const T &l, const T &r) const { return l < r; }
};

template <typename T, class Compare>
void merge(T *arr, size_t l, size_t m, size_t r, Compare comp) {
  T *sorted = new T[r - l + 1];
  size_t left_ind = l, right_ind = m + 1, sorted_ind = 0;

  // Have we reached the end of any array?
  while (left_ind != m + 1 && right_ind != r + 1) {
    if (comp(arr[left_ind], arr[right_ind])) {
      sorted[sorted_ind] = arr[left_ind];
      ++left_ind;

    } else {
      sorted[sorted_ind] = arr[right_ind];
      ++right_ind;
    }

    ++sorted_ind;
  }

  // Copy all remaining elements of non-empty array
  if (left_ind == m + 1) {
    // Copy all remaining elements of right sub-array
    for (size_t i = right_ind; i < r + 1; ++i, ++sorted_ind)
      sorted[sorted_ind] = arr[i];

  } else {
    // Copy all remaining elements of left sub-array
    for (size_t i = left_ind; i < m + 1; ++i, ++sorted_ind)
      sorted[sorted_ind] = arr[i];
  }

  std::memcpy(arr + l, sorted, sizeof(T) * (r - l + 1));
  delete[] sorted;
}

// recursion is used
// The recursion depth of the merge D(n) = std::ceil(log2(n))
// Pretty sure that the number of elements won't exceed 10^301
// Considering the fact, that the number of atoms in the Universe
// is only about 10^80
template <typename T, class Compare = is_less<T>>
void merge_sort(T *arr, size_t l, size_t r, Compare comp = is_less<T>()) {
  if (l < r) {
    size_t m = (r + l) / 2;
    merge_sort(arr, l, m, comp);
    merge_sort(arr, m + 1, r, comp);
    merge<T, Compare>(arr, l, m, r, comp);
  }
}

/////////////////////////////
//      Solving task       //
/////////////////////////////

struct Section {
  size_t begin = 0;
  size_t end = 0;
};

class Number_line {
  struct Point {
    size_t x;
    int8_t dif;
  } *points_arr = nullptr;
  size_t size = 0;

  void alloc_from_sections(Section *arr);

 public:
  Number_line() = default;
  explicit Number_line(Section *_arr, size_t _size);
  ~Number_line() { delete[] points_arr; }

  size_t get_union_length();
};

void Number_line::alloc_from_sections(Section *arr) {
  points_arr = new Point[size];
  for (size_t i = 0; i < (size / 2); ++i) {
    points_arr[2 * i] = {arr[i].begin, 1};
    points_arr[2 * i + 1] = {arr[i].end, -1};
  }

#ifdef DEBUG
  std::cout << "After alloc:\n";
  for (size_t i = 0; i < size; ++i)
    std::cout << points_arr[i].x << " = " << static_cast<int>(points_arr[i].dif)
              << std::endl;
#endif
}

Number_line::Number_line(Section *_arr, size_t _size) : size(_size * 2) {
  alloc_from_sections(_arr);
  merge_sort(points_arr, 0, size - 1, [](const Point &a, const Point &b) {
    return (a.x == b.x) ? a.dif < b.dif : a.x < b.x;
  });

#ifdef DEBUG
  std::cout << "After sort:\n";
  for (size_t i = 0; i < size; ++i)
    std::cout << points_arr[i].x << " = " << static_cast<int>(points_arr[i].dif)
              << std::endl;
#endif
}

size_t Number_line::get_union_length() {
  uint width = 0;
  size_t union_length = 0;
  for (size_t i = 0; i < size; ++i) {
    if (width > 0) union_length += points_arr[i].x - points_arr[i - 1].x;

    points_arr[i].dif == 1 ? ++width : --width;
  }

  return union_length;
}

int main() {
  size_t size = 0;
  Section *sec_arr = nullptr;

  std::cin >> size;
  sec_arr = new Section[size];
  for (size_t i = 0; i < size; ++i)
    std::cin >> sec_arr[i].begin >> sec_arr[i].end;

#ifdef DEBUG
  std::cout << "Before solution:\n";
  for (size_t i = 0; i < size; ++i)
    std::cout << sec_arr[i].begin << ':' << sec_arr[i].end << std::endl;
#endif

  Number_line solution(sec_arr, size);
  std::cout << solution.get_union_length();

  delete[] sec_arr;
  return 0;
}

// Copyright 2021 Zheleztsov N. (ML-12)

// Variant № 4
// Implement the “random element” pivot selection strategy.
// Implement the Partition function by traversing two iterators
// from the end of the array to the beginning.

#include <assert.h>

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <future>
#include <iostream>

// soring in the descending direction
template <typename T>
class is_more {
 public:
  bool operator()(const T &l, const T &r) { return l > r; }
};

// soring in the ascending direction
template <typename T>
class is_less_or_equal {
 public:
  bool operator()(const T &l, const T &r) { return l <= r; }
};

template <typename T, class Compare = is_less_or_equal<T>>
int partition(T *arr, size_t l, size_t r,
              Compare comp = is_less_or_equal<T>()) {
  // Selecting pivot
  std::srand(std::time(0));
  int rand_pivot_pos = (r > l) ? std::rand() % (r - l) + l : l;

  // Put pivot to the beginning of the array
  if (l != rand_pivot_pos) std::swap(arr[l], arr[rand_pivot_pos]);

  // From the end to the beginning
  // i indicates the beginning of the less/equal subarray
  // j indicates the beginning of the unconsidered values
  //
  // -------------------------------------------
  // |pivot| ?????????? | <= <= <= <= | > > > >
  //                  j             i
  // -------------------------------------------

  int i = r, j = r;
  while (j > l)
    if (!comp(arr[l], arr[j]))
      --j;
    else
      std::swap(arr[i--], arr[j--]);

  std::swap(arr[i], arr[l]);
  return i;
}

template <typename T, class Compare = is_less_or_equal<T>>
int k_stat(T *arr, size_t l, size_t r, size_t p,
           Compare comp = is_less_or_equal<T>()) {
  size_t k = ((p / 100.0) * (r + 1));
  int pivot_pos = partition(arr, l, r, comp);

  while (pivot_pos != k) {
    if (k < pivot_pos) {
      r = pivot_pos - 1;
      pivot_pos = partition(arr, l, r, comp);
    } else {
      l = pivot_pos + 1;
      pivot_pos = partition(arr, l, r, comp);
    }
  }

  return arr[k];
}

int main(int argc, char **argv) {
  size_t size = 0;
  std::cin >> size;
  int *arr = new int[size];

  for (size_t i = 0; i < size; ++i) std::cin >> arr[i];

  std::cout << k_stat(arr, 0, size - 1, 10) << std::endl
            << k_stat(arr, 0, size - 1, 50) << std::endl
            << k_stat(arr, 0, size - 1, 90) << std::endl;

  delete[] arr;
  return 0;
}

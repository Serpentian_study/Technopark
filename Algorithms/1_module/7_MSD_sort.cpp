// Copyright 2021 Zheleztsov N. (ML-12)
//
// Variant 7.2
// An array of non-negative 64-bit integers is given.
// The number of numbers is no more than 106.
// Sort the array using the LSD radix sort method

#include <cstring>
#include <iostream>
#include <type_traits>

void radix_sort(uint64_t *a_arr, size_t size, uint8_t digit_ind) {
  auto get_digit{[](uint64_t value, uint8_t digit_ind) -> uint8_t {
    return (value >> (8 * digit_ind)) & 255;
  }};

  const uint16_t k = 256;
  uint64_t *c_arr = new uint64_t[k]();

  // Trivial counting sort
  for (size_t i = 0; i < size; ++i) ++c_arr[get_digit(a_arr[i], digit_ind)];
  // Getting the indexes of the group ends
  for (uint16_t i = 1; i < k; ++i) c_arr[i] += c_arr[i - 1];

  // Getting result
  uint64_t *b_arr = new uint64_t[size];
  for (size_t i = 0; i < size; ++i)
    b_arr[--c_arr[get_digit(a_arr[size - 1 - i], digit_ind)]] =
        a_arr[size - 1 - i];

  memcpy(a_arr, b_arr, size * sizeof(uint64_t));
  delete[] c_arr;
  delete[] b_arr;
}

void LSD_sort(uint64_t *arr, size_t size) {
  for (uint8_t i = 0; i < 8; ++i) {
    radix_sort(arr, size, i);
  }
}

int main() {
  size_t size = 0;
  std::cin >> size;
  uint64_t *arr = new uint64_t[size];
  for (size_t i = 0; i < size; ++i) std::cin >> arr[i];

  // Solving the task
  LSD_sort(arr, size);

  for (size_t i = 0; i < size; ++i) std::cout << arr[i] << ' ';

  delete[] arr;
  return 0;
}

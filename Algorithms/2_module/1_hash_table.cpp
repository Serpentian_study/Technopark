/* Железцов Н. WEB-12
 * Вариант 1

 * Реализуйте структуру данных типа “множество строк” на основе
 * динамической хеш-таблицы с открытой адресацией.
 * Хранимые строки непустые и состоят из строчных латинских букв.
 * Хеш-функция строки должна быть реализована с помощью вычисления
 * значения многочлена методом Горнера. Начальный размер таблицы
 * должен быть равным 8-ми. Перехеширование выполняйте при добавлении
 * элементов в случае, когда коэффициент заполнения таблицы достигает 3/4.
 * Структура данных должна поддерживать операции добавления строки
 * в множество, удаления строки из множества и проверки
 * принадлежности данной строки множеству.
 *
 * 1_1. Для разрешения коллизий используйте квадратичное пробирование.
 * i-ая проба g(k, i)=g(k, i-1) + i (mod m). m - степень двойки.
*/

#include <algorithm>
#include <cassert>
#include <iostream>
#include <string>
#include <type_traits>
#include <vector>

struct HornerHash {
  uint64_t operator()(const std::string& str) const {
    uint64_t hash = 0;
    std::for_each(str.cbegin(), str.cend(),
                  [&hash](typename std::string::const_reference c) {
                    hash = hash * 13 + c;
                  });
    return hash;
  }
};

template <class Key, class Hash = std::hash<Key>,
          class KeyEqual = std::equal_to<Key>>
class HashTable {
 public:
  typedef Key key_type;
  typedef Key value_type;
  typedef size_t size_type;
  typedef Hash hasher;
  typedef KeyEqual key_equal;
  typedef value_type& reference;
  typedef const value_type& const_reference;
  typedef typename std::result_of<hasher(Key)>::type hash_type;

  // Constructs empty container.
  // Sets max_load_factor() to 0.75
  // Sets bucket_count size to 8
  // Sets max del factor to 0.33
  HashTable(size_type bucket_count = 8, const Hash& hash = Hash(),
            const key_equal& equal = key_equal()) noexcept;

  // Not copyable container
  HashTable(const HashTable& other) = delete;
  HashTable& operator=(const HashTable& other) = delete;

  ~HashTable() noexcept = default;

  // Interface
  bool contains(const Key& key) const noexcept;
  bool insert(const value_type& value) noexcept;
  bool erase(const Key& key) noexcept;

 private:
  class Cell_ {
   public:
    enum class State { EMPTY, DELETED, DATA };

    Cell_() noexcept = default;
    explicit Cell_(State state, const value_type &data, hash_type hash)
        : _node_state(state), _hash(hash), _data(data) {}

    // Modifiers
    void erase() noexcept { _node_state = State::DELETED; }

    // Getters
    State get_state() const noexcept { return _node_state; }
    value_type get_data() const noexcept { return _data; }
    hash_type get_hash() const noexcept { return _hash; }
    bool operator==(const Cell_& rhs) const noexcept {
      return _node_state == rhs._node_state && _data == rhs._data &&
             _hash == rhs._hash;
    }

   private:
    State _node_state = State::EMPTY;
    hash_type _hash = 0;
    value_type _data;
  };

  bool no_hash_insert(std::vector<Cell_>& to, const Cell_& cell) noexcept;
  void rehash(size_t new_size) noexcept;

  // Pretty obvious ones:
  hasher hash_;
  key_equal equal_;
  std::vector<Cell_> bucket_;

  // number of cells with data
  size_type data_num_;
  // number of deleted cells
  size_type del_num_;
  // data_num_ / bucket_.size()
  float max_load_factor_;
  // del_num_ / bucket_.size()
  float max_del_factor_;
};

template <class Key, class Hash, class KeyEqual>
HashTable<Key, Hash, KeyEqual>::HashTable(size_type bucket_count,
                                          const Hash& hash,
                                          const key_equal& equal) noexcept
    : hash_(hash),
      equal_(equal),
      bucket_(bucket_count, Cell_()),
      data_num_(0),
      del_num_(0),
      max_load_factor_(0.75),
      max_del_factor_(0.33) {}

template <class Key, class Hash, class KeyEqual>
bool HashTable<Key, Hash, KeyEqual>::contains(const Key& key) const noexcept {
  uint64_t hash = hash_(key) % bucket_.size();
  // quadratic probing
  for (size_t idx = hash, i = 0; i < bucket_.size();
       ++i, idx = (idx + i) % bucket_.size()) {
    auto state = bucket_[idx].get_state();
    if (state == Cell_::State::EMPTY) {
      return false;

    } else if (state == Cell_::State::DELETED) {
      continue;

    } else if (state == Cell_::State::DATA) {
      if (equal_(bucket_[idx].get_data(), key)) {
        return true;
      } else {
        continue;
      }

    } else {
      assert(false);
    }
  }

  return false;
}

template <class Key, class Hash, class KeyEqual>
bool HashTable<Key, Hash, KeyEqual>::insert(const value_type& value) noexcept {
  if (data_num_ > max_load_factor_ * bucket_.size()) {
    rehash(bucket_.size() * 2);
  }

  // Cell filling;
  Cell_ cell(Cell_::State::DATA, value, hash_(value));
  return no_hash_insert(bucket_, cell);
}

template <class Key, class Hash, class KeyEqual>
bool HashTable<Key, Hash, KeyEqual>::erase(const Key& key) noexcept {
  hash_type hash = hash_(key) % bucket_.size();
  for (size_t idx = hash, i = 0; i < bucket_.size();
       ++i, idx = (idx + i) % bucket_.size()) {
    auto state = bucket_[idx].get_state();
    if (state == Cell_::State::EMPTY) {
      return false;

    } else if (state == Cell_::State::DELETED) {
      continue;

    } else if (state == Cell_::State::DATA) {
      if (equal_(bucket_[idx].get_data(), key)) {
        bucket_[idx].erase();
        --data_num_;
        ++del_num_;
        if (del_num_ > max_del_factor_ * bucket_.size()) {
          rehash(bucket_.size());
        }

        return true;

      } else {
        continue;
      }

    } else {
      assert(false);
    }
  }

  return false;
}

template <class Key, class Hash, class KeyEqual>
bool HashTable<Key, Hash, KeyEqual>::no_hash_insert(
    std::vector<Cell_>& to, const Cell_& cell) noexcept {
  hash_type hash = cell.get_hash() % to.size();
  // in order to escape overflow
  bool found_del = false;
  size_t pos_of_first_del;

  for (size_t idx = hash, i = 0; i < to.size();
       ++i, idx = (idx + i) % to.size()) {
    auto state = to[idx].get_state();
    if (state == Cell_::State::EMPTY) {
      if (!found_del) {
        to[idx] = cell;
      } else {
        to[pos_of_first_del] = cell;
      }
      ++data_num_;
      return true;

    } else if (state == Cell_::State::DELETED) {
      if (!found_del) {
        pos_of_first_del = idx;
        found_del = true;
      }
      continue;

    } else if (state == Cell_::State::DATA) {
      if (equal_(to[idx].get_data(), cell.get_data())) {
        return false;
      } else {
        continue;
      }

    } else {
      assert(false);
    }
  }

  // if no empty:
  if (found_del) {
    to[pos_of_first_del] = cell;
    ++data_num_;
    return true;
  }

  // we're not supposed to be here
  assert(false);
}

template <class Key, class Hash, class KeyEqual>
void HashTable<Key, Hash, KeyEqual>::rehash(size_t new_size) noexcept {
  std::vector<Cell_> temp(new_size, Cell_());
  for (size_t i = 0; i < bucket_.size(); ++i) {
    if (bucket_[i].get_state() == Cell_::State::DATA) {
      bool succes = no_hash_insert(temp, bucket_[i]);
      assert(succes);
    }
  }

  bucket_ = std::move(temp);
  del_num_ = 0;
}

int main() {
  HashTable<std::string, HornerHash> hash_table;
  char oper;
  std::string data;
  while (std::cin >> oper >> data) {
    switch (oper) {
      case '+':
        std::cout << (hash_table.insert(data) ? "OK" : "FAIL") << std::endl;
        break;

      case '-':
        std::cout << (hash_table.erase(data) ? "OK" : "FAIL") << std::endl;
        break;

      case '?':
        std::cout << (hash_table.contains(data) ? "OK" : "FAIL") << std::endl;
        break;

      default:
        return EXIT_FAILURE;
    }
  }

  return (EXIT_SUCCESS);
}

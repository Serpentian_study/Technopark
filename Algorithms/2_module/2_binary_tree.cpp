/*
 * Железцов Н. ML-12
 * Вариант 2
 *
 * Дано число N < 10^6 и последовательность целых чисел из [-231..231] длиной N.
 * Требуется построить бинарное дерево, заданное наивным порядком вставки.
 * Т.е., при добавлении очередного числа K в дерево с корнем root,
 * если root→Key ≤ K, то узел K добавляется в правое поддерево root;
 * иначе в левое поддерево root. Выведите элементы в порядке pre-order (сверху
 * вниз). Рекурсия запрещена.
 */

#include <functional>
#include <iostream>
#include <stack>

template <class T, class Comp = std::greater<T>>
class BinaryTree {
  template <class L>
  struct Node {
    L data_;
    Node<L>* right_;
    Node<L>* left_;

    Node() : right_(nullptr), left_(nullptr), data_(L()) {}
    explicit Node(const T& data, Node* right = nullptr, Node* left = nullptr)
        : data_(data), right_(right), left_(left) {}
    ~Node() = default;
  };

 public:
  typedef Node<T>* NodePtr;

  BinaryTree() noexcept : comp_(), root_(nullptr) {}
  BinaryTree(const BinaryTree&) = delete;
  BinaryTree& operator=(const BinaryTree&) = delete;
  ~BinaryTree() noexcept;

  void push(const T& value) noexcept;

  template <class K, class C>
  friend std::ostream& operator<<(std::ostream& out,
                                  const BinaryTree<K, C>& tree);

 private:
  typedef std::function<void(NodePtr)> Func;
  void pre_order_traversal(Func func) const noexcept;

  Comp comp_;
  NodePtr root_;
};

template <class T, class Comp>
void BinaryTree<T, Comp>::push(const T& value) noexcept {
  // in order to capture pointer on itself and not copy of it
  auto temp = &root_;
  while (true) {
    if (!(*temp)) {
      *temp = new Node<T>(value);
      return;
    }

    if (comp_((*temp)->data_, value)) {
      temp = &(*temp)->left_;
    } else {
      temp = &(*temp)->right_;
    }
  }
}

template <class T, class Comp>
void BinaryTree<T, Comp>::pre_order_traversal(
    BinaryTree::Func func) const noexcept {
  if (!root_) {
    return;
  }

  std::stack<NodePtr> stack;
  NodePtr temp = root_;

  while (true) {
    while (temp) {
      func(temp);
      stack.push(temp);
      temp = temp->left_;
    }

    if (!stack.empty()) {
      temp = stack.top()->right_;
      stack.pop();
    }

    if (!temp && stack.empty()) {
      break;
    }
  }
}

template <class T, class Comp>
BinaryTree<T, Comp>::~BinaryTree() noexcept {
  if (!root_) {
    return;
  }

  // deleting leafs firstly
  // get pointer itself and not his copy
  std::stack<NodePtr*> stack;
  stack.push(&root_);

  while (true) {
    auto temp = *stack.top();
    if (temp->left_) {
      stack.push(&(temp->left_));
      continue;
    }

    if (temp->right_) {
      stack.push(&(temp->right_));
      continue;
    }

    delete temp;
    *stack.top() = nullptr;
    stack.pop();
    if (stack.empty()) {
      return;
    }
  }
}

template <class T, class Comp = std::less_equal<T>>
std::ostream& operator<<(std::ostream& out, const BinaryTree<T, Comp>& tree) {
  tree.pre_order_traversal([&out](typename BinaryTree<T, Comp>::NodePtr node) {
    out << node->data_ << ' ';
  });

  return out;
}

int main() {
  uint32_t num = 0;
  std::cin >> num;

  BinaryTree<int64_t> tree;
  for (uint32_t i = 0; i < num; ++i) {
    int64_t temp = 0;
    std::cin >> temp;
    tree.push(temp);
  }

  std::cout << tree << std::endl;

  return EXIT_SUCCESS;
}

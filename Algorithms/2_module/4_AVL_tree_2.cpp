/* Железцов Н. Ml-12
 * Вариант 1
 *
 *
 * В одной военной части решили построить в одну шеренгу по росту.
 * Т.к. часть была далеко не образцовая, то солдаты часто приходили
 * не вовремя, а то их и вовсе приходилось выгонять из шеренги за
 * плохо начищенные сапоги. Однако солдаты в процессе прихода и ухода
 * должны были всегда быть выстроены по росту – сначала самые высокие,
 * а в конце – самые низкие. За расстановку солдат отвечал прапорщик,
 * который заметил интересную особенность – все солдаты в части
 * разного роста. Ваша задача состоит в том, чтобы помочь прапорщику
 * правильно расставлять солдат, а именно для каждого приходящего
 * солдата указывать, перед каким солдатом в строе он должен становится.
 * Требуемая скорость выполнения команды - O(log n). *
 *
 */

/*
 * P.S. Использовал АВЛ дерево, написанное полгода назад для мапы:
 * https://github.com/NikZheleztsov/Map-implementation
 * (убрал лишнее, добавил подсчет статистик)
 */

#include <cassert>
#include <cstddef>
#include <iostream>
#include <stack>

////////////////////////////////////////
//             node.hpp               //
////////////////////////////////////////

template <typename Key>
struct Node {
  Key first;

  Node()
      : first(Key()),
        top(nullptr),
        left(nullptr),
        right(nullptr),
        dif(0),
        height(0),
        nodes_num(1) {}

  Node(Key key, Node* Top, Node* Left, Node* Right, int Dif, uint64_t h,
       uint64_t num)
      : first(key),
        top(Top),
        left(Left),
        right(Right),
        dif(Dif),
        height(h),
        nodes_num(num) {}

  bool operator==(Node<Key> nd) {
    return (first = nd.first && top == nd.top && left == nd.left &&
                    right == nd.right && dif == nd.dif && height == nd.height,
            nodes_num = nd.nodes_num);
  }

 private:
  Node* top;
  Node* left;
  Node* right;
  int dif;  // right - left
  uint64_t height;
  uint64_t nodes_num;

  // in order to limit user access to private members
  template <typename, class, class>
  friend class Tree;
};

////////////////////////////////////////
//           avl_tree.hpp             //
////////////////////////////////////////

template <typename Key, class Comp = std::greater<Key>,
          class Allocator = std::allocator<Node<Key> > >
class Tree {
  Node<Key>* root = nullptr;
  Allocator alloc = Allocator();
  size_t _size;

  struct tuple {
    Node<Key>* min;
    Node<Key>* subtree;

    tuple(Node<Key>* a, Node<Key>* b) : min(a), subtree(b) {}
  };

  void recalculate(Node<Key>* nd);
  Node<Key>* small_l_rot(Node<Key>* nd);
  Node<Key>* small_r_rot(Node<Key>* nd);
  Node<Key>* big_l_rot(Node<Key>* nd);
  Node<Key>* big_r_rot(Node<Key>* nd);
  Node<Key>* balance(Node<Key>* nd, Node<Key>* top);
  Node<Key>* find_in(Node<Key>* nd, const size_t& idx);
  Node<Key>* push_in(Node<Key>* nd, const Key& pair, Node<Key>* top);
  Node<Key>* del_in(Node<Key>* nd, const Key& idx, Node<Key>* top);
  tuple find_and_del_right_min(Node<Key>* nd, Node<Key>* top);

 public:
  typedef typename std::allocator_traits<Allocator> alloc_traits;
  typedef Node<Key>* pointer;

  Tree() : root(nullptr), _size(0) {}
  Tree(const Tree<Key, Comp, Allocator>& other) = delete;

  Node<Key>* find_idx(const size_t& idx) noexcept;
  void push(const Key& key) noexcept;
  void del(const Key& key) noexcept;

  ~Tree();
};

// calculating height and nodes_num based on bottom Nodes
template <typename Key, class Comp, class Allocator>
void Tree<Key, Comp, Allocator>::recalculate(Node<Key>* nd) {
  if (nd) {
    if (nd->right != nullptr && nd->left != nullptr) {
      nd->height = std::max(nd->right->height, nd->left->height) + 1;
      nd->dif = nd->right->height - nd->left->height;
      nd->nodes_num = nd->right->nodes_num + nd->left->nodes_num + 1;

    } else if (nd->right == nullptr && nd->left != nullptr) {
      nd->height = nd->left->height + 1;
      nd->dif = 0 - nd->left->height;
      nd->nodes_num = nd->left->nodes_num + 1;

    } else if (nd->right != nullptr && nd->left == nullptr) {
      nd->height = nd->right->height + 1;
      nd->dif = nd->right->height;
      nd->nodes_num = nd->right->nodes_num + 1;

    } else if (nd->right == nullptr && nd->left == nullptr) {
      nd->height = 1;
      nd->dif = 0;
      nd->nodes_num = 1;
    }
  }
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::small_l_rot(Node<Key>* nd) {
  Node<Key>* temp = nd->right;

  nd->right = temp->left;
  if (temp->left) temp->left->top = nd;

  temp->left = nd;
  nd->top = temp;
  nd = temp;

  return nd;
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::small_r_rot(Node<Key>* nd) {
  Node<Key>* temp = nd->left;

  nd->left = temp->right;
  if (temp->right) temp->right->top = nd;

  temp->right = nd;
  nd->top = temp;
  nd = temp;

  return nd;
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::big_l_rot(Node<Key>* nd) {
  nd->right = small_r_rot(nd->right);
  nd = small_l_rot(nd);
  return nd;
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::big_r_rot(Node<Key>* nd) {
  nd->left = small_l_rot(nd->left);
  nd = small_r_rot(nd);

  return nd;
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::balance(Node<Key>* nd, Node<Key>* top) {
  if (nd) {
    recalculate(nd);

    bool root_flag = 0;
    if (nd == root) root_flag = 1;

    if (nd->dif == 2 &&
        ((nd->right == nullptr ? 1 : (nd->right->dif == 0 ? 1 : 0)) ||
         (nd->right == nullptr ? 0 : (nd->right->dif == 1 ? 1 : 0)))) {
      nd = small_l_rot(nd);
      nd->top = top;
      recalculate(nd->left);
      recalculate(nd);

      if (root_flag) root = nd;
    }

    if (nd->right != nullptr) {
      if (nd->dif == 2 && nd->right->dif < 0) {
        nd = big_l_rot(nd);
        nd->top = top;
        recalculate(nd->left);
        recalculate(nd->right);
        recalculate(nd);

        if (root_flag) root = nd;
      }
    }

    if (nd->dif == -2 &&
        ((nd->left == nullptr ? 1 : (nd->left->dif == 0 ? 1 : 0)) ||
         (nd->left == nullptr ? 0 : (nd->left->dif == -1 ? 1 : 0)))) {
      nd = small_r_rot(nd);
      nd->top = top;
      recalculate(nd->right);
      recalculate(nd);

      if (root_flag) root = nd;
    }

    if (nd->left != nullptr) {
      if (nd->dif == -2 && nd->left->dif > 0) {
        nd = big_r_rot(nd);
        nd->top = top;
        recalculate(nd->left);
        recalculate(nd->right);
        recalculate(nd);

        if (root_flag) root = nd;
      }
    }
  }

  return nd;
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::push_in(Node<Key>* nd, const Key& pair,
                                               Node<Key>* top) {
  if (nd == nullptr) {
    nd = alloc_traits::allocate(alloc, 1);
    alloc_traits::construct(alloc, nd, pair, top, nullptr, nullptr, 0, 1, 1);
    _size++;
    return nd;

  } else if (nd->first == pair) {
    // similar keys are forbidden
    assert(false);

  } else if (Comp{}(pair, nd->first)) {
    nd->right = push_in(nd->right, pair, nd);
    return balance(nd, top);

  } else {
    nd->left = push_in(nd->left, pair, nd);
    return balance(nd, top);
  }
}

template <typename Key, class Comp, class Allocator>
void Tree<Key, Comp, Allocator>::push(const Key& value) noexcept {
  root = push_in(root, value, nullptr);
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::find_idx(const size_t& idx) noexcept {
  return find_in(root, idx);
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::find_in(Node<Key>* nd, const size_t& idx) {
  if (!nd) {
    return nullptr;
  }

  size_t curr_stat = (nd->left) ? nd->left->nodes_num : 0;
  if (idx == curr_stat) {
    return nd;
  }

  if (Comp{}(idx, curr_stat)) {
    return find_in(nd->right, idx - curr_stat - 1);
  } else {
    return find_in(nd->left, idx);
  }
}

template <typename Key, class Comp, class Allocator>
Tree<Key, Comp, Allocator>::~Tree() {
  if (!root) {
    return;
  }

  std::stack<Node<Key>**> stack;
  stack.push(&root);

  while (true) {
    auto temp = *stack.top();
    if (temp->left) {
      stack.push(&(temp->left));
      continue;
    }

    if (temp->right) {
      stack.push(&(temp->right));
      continue;
    }

    alloc_traits::deallocate(alloc, temp, 1);
    *stack.top() = nullptr;
    stack.pop();
    if (stack.empty()) {
      return;
    }
  }
}

template <typename Key, class Comp, class Allocator>
typename Tree<Key, Comp, Allocator>::tuple
Tree<Key, Comp, Allocator>::find_and_del_right_min(Node<Key>* nd,
                                                   Node<Key>* top) {
  if (!nd->left) {
    return tuple(nd, nd->right);
  } else {
    tuple tup = find_and_del_right_min(nd->left, nd);
    nd->left = tup.subtree;
    return tuple(tup.min, balance(nd, top));
  }
}

template <typename Key, class Comp, class Allocator>
void Tree<Key, Comp, Allocator>::del(const Key& key) noexcept {
  root = del_in(root, key, nullptr);
}

template <typename Key, class Comp, class Allocator>
Node<Key>* Tree<Key, Comp, Allocator>::del_in(Node<Key>* nd, const Key& key,
                                              Node<Key>* top) {
  assert(nd);
  if (nd->first == key) {
    auto left = nd->left;
    auto right = nd->right;

    alloc_traits::deallocate(alloc, nd, 1);
    if (!right) {
      return left;
    }

    tuple tuple = find_and_del_right_min(right, nd);
    tuple.min->right = tuple.subtree;
    tuple.min->top = top;
    tuple.min->left = left;
    return balance(tuple.min, top);

  } else if (Comp{}(key, nd->first)) {
    nd->right = del_in(nd->right, key, nd);
    return balance(nd, top);

  } else if (!Comp{}(key, nd->first)){
    nd->left = del_in(nd->left, key, nd);
    return balance(nd, top);
  }

  // no key found
  assert(false);
  return nullptr;
}

int main() {
  Tree<uint32_t> tree;
  uint32_t num = 0;
  std::cin >> num;

  for (int i = 0; i < num; ++i) {
    int64_t value, k_stat;
    std::cin >> value >> k_stat;
    if (value > 0) {
      tree.push(value);
    } else {
      tree.del(value * (-1));
    }

    auto nd = tree.find_idx(k_stat);
    assert(nd);

    std::cout << nd->first << ' ';
  }

  return EXIT_SUCCESS;
}
